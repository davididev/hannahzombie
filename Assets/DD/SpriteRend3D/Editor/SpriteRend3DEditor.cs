﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteRend3D))]
public class SpriteRend3DEditor : Editor
{

    private bool firstDirty = true;
    public override void OnInspectorGUI()
    {
        SpriteRend3D settings = (SpriteRend3D) target;

        serializedObject.Update();
        GUILayout.Label("Please note- this component needs to be \na child of the collider/rigid body controlling it.");
        GUILayout.Label("It must also be on the same gameobject \nas a Sprite Renderer.");


        EditorGUILayout.LabelField("Lock: ");
        settings.lockX = EditorGUILayout.Toggle("X", settings.lockX);
        settings.lockY = EditorGUILayout.Toggle("Y", settings.lockY);
        settings.lockZ = EditorGUILayout.Toggle("Z", settings.lockZ);



        settings.eightDirections = EditorGUILayout.Toggle("8 directions?", settings.eightDirections);
        if(settings.eightDirections)
        {
            GetProperty("rend0");
            GetProperty("rend45");
            GetProperty("rend90");
            GetProperty("rend135");
            GetProperty("rend180");
            GetProperty("rend225");
            GetProperty("rend270");
            GetProperty("rend315");
        }
        else
        {
            GetProperty("rend0");
        }


        GUILayout.Label("Colors: ");
        GetProperty("emmissive");
        GetProperty("diffuse");

        serializedObject.ApplyModifiedProperties();
    }

    void GetProperty(string name)
    {
        SerializedProperty prop;
        prop = serializedObject.FindProperty(name);
        EditorGUILayout.PropertyField(prop);
    }
}
