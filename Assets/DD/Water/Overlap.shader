﻿Shader "Davideyes/OverlapTexture"
{
    Properties
    {
		_MainTex("Bottom Texture", 2D) = "white" {}
		_Color("Bottom Color", Color) = (1, 1, 1, 1)
		_OverlapTex("Top Texture", 2D) = "white" {}
		_OverlapColor("Top Color", Color) = (1, 1, 1, 0.5)
		_EmmissionColor("Emmission", Color) = (0, 0, 0, 0)
        _Glossiness ("Smoothness", Range(0,1)) = 0.0
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
	
        Tags { "RenderType"="Fade" "Queue" = "Transparent" }
        LOD 200
		Blend One OneMinusSrcAlpha
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _OverlapTex;
		fixed4 _OverlapColor;
		fixed4 _EmmissionColor;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_OverlapTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
			fixed4 c1 = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = tex2D(_OverlapTex, IN.uv_OverlapTex) * _OverlapColor;
			fixed4 c = lerp(c1, c2, c2.a);
            o.Albedo = c;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
			o.Emission = fixed3(_EmmissionColor.r, _EmmissionColor.g, _EmmissionColor.b);
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
