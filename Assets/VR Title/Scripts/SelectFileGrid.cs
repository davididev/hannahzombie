﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectFileGrid : MonoBehaviour
{
    public GameObject newFileTemplete;
    private bool firstTime = true;
    // Start is called before the first frame update
    void OnEnable()
    {
        if(firstTime)
        {
            Vector3 startingAnchor = Vector3.zero;

            int i = 0;
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if(x == 0 && y == 0)
                    {
                        //First file
                        startingAnchor = newFileTemplete.GetComponent<RectTransform>().localPosition;
                        newFileTemplete.GetComponent<NewFileButton>().SetFileID(0);
                    }
                    else
                    {
                        GameObject g = GameObject.Instantiate(newFileTemplete, newFileTemplete.transform.parent);
                        Vector3 v = startingAnchor;
                        v.x += (140f * x);
                        v.y -= (140f * y);
                        g.GetComponent<RectTransform>().localPosition = v;
                        
                        g.GetComponent<NewFileButton>().SetFileID(i);
                    }
                    i++;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
