﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTitle : MonoBehaviour
{
    public GameObject newFile, selectFile, fileSelectedPanel;
    public TextAsset creditsFile;
    public TMPro.TextMeshProUGUI creditsTxt;

    // Start is called before the first frame update
    void Start()
    {
        BoilerRoot.DestroyMe();
        Time.timeScale = 1f;
        
        StartCoroutine(CreditsRoutine());
        StartCoroutine(SettingPanel());
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator SettingPanel()
    {
        //Don't show any panels until the brush is active
        yield return new WaitForEndOfFrame();
        SetPanelID(-1);
        while (TitleBrush.isWorking == false)
        {
            yield return new WaitForEndOfFrame();
        }
        SetPanelID(0);
    }

    public void SetPanelID(int id)
    {
        selectFile.SetActive(id == 0);
        newFile.SetActive(id == 1);
        fileSelectedPanel.SetActive(id == 2);
    }

    IEnumerator CreditsRoutine()
    {
        string[] lines = creditsFile.text.Split('*');
        while (gameObject)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                creditsTxt.text = lines[i];
                yield return new WaitForSecondsRealtime(10f);
            }
        }
}

    // Update is called once per frame
    void Update()
    {
        
    }
}
