﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class FileSelectedPanel : MonoBehaviour
{
    public GameObject mainPanel, confirmDelete, confirmMission;
    public TMPro.TextMeshProUGUI chapter1Text, chapter2Text;
    int missionID = 0;
    // Start is called before the first frame update
    void OnEnable()
    {
        GameData.instance.Load();
        int totalScore = 0;
        for(int i = 0; i < GameData.instance.highScores.Length; i++)
        {
            totalScore += GameData.instance.highScores[i];
        }
        chapter1Text.text = chapter1Text.text.Replace("%s", totalScore.ToString() + "/600");

        totalScore = 0;
        for (int i = 0; i < GameData.instance.highScores2.Length; i++)
        {
            totalScore += GameData.instance.highScores2[i];
        }
        chapter2Text.text = chapter2Text.text.Replace("%s", totalScore.ToString() + "/700");
        SetPanelID(0);

    }

    public void Continue()
    {
        LoadingUI.sceneID = GameData.instance.levelID;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
    }

    private void LoadChapter1()
    {
        
        GameData.instance.levelID = 2;
    }

    private void LoadChapter2()
    {
        GameData.instance.levelID = 9;
        
    }

    /// <summary>
    /// Start the chapter over again.
    /// </summary>
    public void LoadChapter()
    {
		for(int i = 0; i < 3; i++)
		{
			PlayerGun.ammo[i] = 0;
			
        }
		GameData.instance.Save();
		
        if (missionID == 1)
            LoadChapter1();
        if (missionID == 2)
            LoadChapter2();

        
		Continue();
    }

    public void SetPanelID(int id)
    {
        mainPanel.SetActive(id == 0);
        confirmDelete.SetActive(id == 1);
        confirmMission.SetActive(id >= 2 && id <= 3);
        if (id == 2)  //Chapter 1
            missionID = 1;
        if (id == 3)  //Chapter 2
            missionID = 2;
    }

    public void GoBack()
    {
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }
    
    public void DeleteFile()
    {
        int id = GameData.fileID;
        string path1 = Application.persistentDataPath + "/file" + id + ".png";
        string path2 = Application.persistentDataPath + "/file" + id + ".csv";
        File.Delete(path1);
        if (File.Exists(path2))
            File.Delete(path2);

        FindObjectOfType<VRTitle>().SetPanelID(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
