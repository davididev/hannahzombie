﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AFMessage : MonoBehaviour
{
	public AudioClip afmessage;
    // Start is called before the first frame update
    void Start()
    {
        System.DateTime dt = System.DateTime.Now;
		
		if(dt.Month == 4 && dt.Day == 1)
		{
			StartCoroutine(Message());
		}
    }
	
	IEnumerator Message()
	{
		yield return new WaitForSecondsRealtime(1f);
		while(TitleBrush.isWorking == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		AudioSource.PlayClipAtPoint(afmessage, transform.position);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
