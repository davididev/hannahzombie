Graphics:
Rusty Metal Texture set by p0ss @ opengameart.org
Seamless assorted textures 2 by Brian MacIntosh 
Smoke particles by Kenney @ opengameart.org
*Graphics:
Zombie by Rosswet Mobile  @ opengameart.org
Backpack by hc @ opengameart.org
Keycard by codeinfernogames @ opengameart.org
*Graphics:
Labrynth textures by OnyxSystems @ opengameart.org
4 metal plates by Georges "TRaK" Grondin @ opengameart.org
Dark brick wall by Keith333 @ opengameart.org
*Graphics:
Skyboxes by Unity Standard assets
Furniture by "Kenney.nl" or "www.kenney.nl" (and opengameart.org)
Low poly skull by TMPxyz@ opengameart.org
*Graphics:
Barrel by bart @ opengameart.org
Spark by Keith333 @ opengameart.org
Quake-like textures by dreameater-games @ itch.io
*Graphics:
Ghoul by TearOfTheStar @ opengameart.org
Rotten wood by Micah Talkiewicz @ opengameart.org
Skull image by Angry Amish @ opengameart.org

*Fonts:
Deutsch Gothic by James Fordyce @ dafont.com

*Audio:
Pistol & Shotgun sound fx by Michel Baradari @ opengameart.org
Rocket sound by Michel Baradari @ opengameart.org
Slice fx by Socapex @ opengameart.org
*Audio:
Gun load sound by pauliuw @ opengameart.org
Horror sound fx by Little Robot Sound Factory @ opengameart.org
www.littlerobotsoundfactory.com
*Audio:
Random SFX by Écrivain @ opengameart.org
Computer beeps by Michel Baradari @ opengameart.org
https://www.text2speech.org/
*Audio:
Water draining by coltures @ freesound.org

*Music:
Horror loop by TinyWorlds @ opengameart.org
Play with demon by Varon Kein @ opengameart.org
Insistent background by yd @ opengameart.org
*Music:
Espionage by brandon75689 @ opengameart.org
Sodium Vaper by Eric Matyas @ opengameart.org