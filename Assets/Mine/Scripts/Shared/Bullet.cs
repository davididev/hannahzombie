﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public string explosionPrefabName;
    public GameObject explosionPrefab;
    public Vector3 relForce;

    public float touchDamage = 10;
    public LayerMask charactersToEffect;
    public Material decal;
    private Rigidbody rigid;
    // Start is called before the first frame update
    void OnEnable()
    {
        if(explosionPrefab != null)
        {
            GameObjectPool.InitPoolItem(explosionPrefabName, explosionPrefab, 10);
        }
    }

    // Update is called once per
    void FixedUpdate()
    {
            rigid = GetComponent<Rigidbody>();

        Vector3 vel = Vector3.zero;
        vel += transform.forward * relForce.z;
        vel += transform.right * relForce.x;
        vel += transform.up * relForce.y;

        rigid.velocity = vel;
    }

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.collider.gameObject.layer;
        int layermask = charactersToEffect.value;
        if (layermask== (layermask | (1 << layer)))
        {
            Health h = collision.collider.GetComponent<Health>();
            if(h != null)
            {
                DmgMessage dm = new DmgMessage(collision.contacts[0].point, collision.contacts[0].normal, touchDamage);
                h.Damage(dm);
            }

            if (explosionPrefab != null)
            {
                GameObjectPool.GetInstance(explosionPrefabName, transform.position, transform.rotation);
            }
            if(decal != null)
            {
                GameObject d = GameObjectPool.GetInstance("decal", Vector3.zero, Quaternion.identity);
                d.GetComponent<Decal>().SetupDecal(collision.contacts[0].point, collision.contacts[0].normal, decal, 3f, 2f, 0f, Decal.KillType.INSTANT_HIDE);
            }
            gameObject.SetActive(false);
        }
    }
}
