﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgMessage
{
    public float amount = 0f;
    public Vector3 hitPosition, normal;

    public DmgMessage(Vector3 pos, Vector3 n, float amt)
    {
        hitPosition = pos;
        normal = n;
        amount = amt;
    }
}
