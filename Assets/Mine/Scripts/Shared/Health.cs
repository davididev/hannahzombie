﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float maxHealth = 100;
    private float health;
    public bool isPlayer = false, bleed = true, isInvincible = false;
	public AudioClip damageSound, dieSound;
	private float hurtTimer = 0f;
	
    public float healthPercentage
    {
        get
        {
            float f = health / maxHealth ;
                return f;
        }
    }

    public static float PlayerHealth = 100f;
    // Start is called before the first frame update
    void OnEnable()
    {
        health = maxHealth;
        
    }
	
	void Start()
	{
		if(isPlayer)
			SceneManager.activeSceneChanged += this.OnReload;
	}
	
	void OnReload(Scene s1, Scene s2)
	{
		health = maxHealth;
        if(isPlayer)
        {
            PlayerHealth = health;
            PlayerUI.updateText = true;
        }
	}

    public void Damage(DmgMessage msg)
    {
        if (isInvincible)
            return;
		if(health <= 0f)
		{
			return;
			//Already dead, no more processing.
		}
		if(bleed)
		{
			GameObject blood = GameObjectPool.GetInstance("blood", msg.hitPosition, Quaternion.identity);
			if(blood != null)
				blood.transform.up = msg.normal;
		}
        health -= msg.amount;

        if(health <= 0f)
        {
            if(dieSound != null)
			{
				AudioSource.PlayClipAtPoint(dieSound, transform.position);
				hurtTimer = dieSound.length;
			}
            gameObject.SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
        }
		else
		{
			if(damageSound != null && hurtTimer <= 0f)
			{
				AudioSource.PlayClipAtPoint(damageSound, transform.position);
				hurtTimer = damageSound.length;
			}
		}
        if(isPlayer)
		{
            int multiplier = Mathf.CeilToInt(msg.amount / 5f);
            if(msg.amount < 1f)
				Finch.FinchController.Right.HapticPulse(10);
			else
			{
				if(multiplier == 1)
					Finch.FinchController.Right.HapticPulse(100);
				if (multiplier == 2)
					Finch.FinchController.Right.HapticPulse(200);
				if (multiplier == 3)
					Finch.FinchController.Right.HapticPulse(300);
				if (multiplier == 4)
					Finch.FinchController.Right.HapticPulse(400);
				if (multiplier == 5)
					Finch.FinchController.Right.HapticPulse(500);
				if (multiplier > 5)
					Finch.FinchController.Right.HapticPulse(1000);
			}
            PlayerHealth = health;
			PlayerUI.updateText = true;
		}
    }

    /// <summary>
    /// Heal the character.
    /// </summary>
    /// <param name="amt">Amount to heal</param>
    /// <returns>true if it healed, false if already maxed out</returns>
    public bool Heal(float amt)
    {
        if (health >= maxHealth)
            return false;

        health += amt;
        if(health >= maxHealth)
        {
            health = maxHealth;
        }

        if(isPlayer)
		{
			PlayerHealth = health;
			PlayerUI.updateText = true;
		}
        return true;
    }

    // Update is called once per frame
    void Update()
    {
		if(hurtTimer > 0f)
			hurtTimer -= Time.deltaTime;
    }
}
