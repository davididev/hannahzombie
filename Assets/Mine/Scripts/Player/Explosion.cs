﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public const float DAMAGE_PER_SECOND = 15f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 normal = other.transform.position - transform.position;
        DmgMessage msg = new DmgMessage(other.transform.position, normal, DAMAGE_PER_SECOND * Time.deltaTime);

        Health h = other.GetComponent<Health>();
        if(h != null)
        {
            h.Damage(msg);
        }
    }
}
