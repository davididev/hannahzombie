﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public static int selectedID = 0;
    public static bool hasKey = false, goal = false;
    public TMPro.TextMeshProUGUI[] ammoTextObjs;
    public TMPro.TextMeshProUGUI zombieText, goalText;
    public Image healthOverlay;
    public GameObject keyDont, keyHave, goalPanel;



    // Start is called before the first frame update
    void Start()
    {
        goal = false;
        goalPanel.SetActive(false);
		SceneManager.activeSceneChanged += this.ResetScene;
    }
	public void ResetScene(Scene prev, Scene next)
	{
		selectedID = 0;
		updateText = true;
		Enemy.totalZombies = 0;
		Enemy.aliveZombies = 0;
		Enemy.alertZombies = 0;
	    hasKey = false;
	}

    public static bool updateText = true;
    // Update is called once per frame
    void Update()
    {
        if(updateText == true)
        {
            UpdateAmmoText();
            if (goal == true)
                FinishLevel();

            updateText = false;
        }
    }

    void FinishLevel()
    {
		goal = false;
        PlayerGun.uiMode = true;
        goalPanel.SetActive(true);
        string s = goalText.text;
        int score = 0;
        if (Enemy.aliveZombies == 0)
            score = 100;
        else
            score = 100 * (Enemy.totalZombies - Enemy.aliveZombies) / Enemy.totalZombies;


        int levelID = GameData.instance.levelID;
        int highScore = 0;
        if (levelID < 8)  //Chapter 1
        {
            highScore = GameData.instance.highScores[levelID - 2];
            if (highScore < score)
                highScore = score;

            GameData.instance.highScores[levelID - 2] = highScore;
        }
		if(levelID >= 8)  //Chapter 2
		{
			highScore = GameData.instance.highScores2[levelID - 9];
            if (score > highScore)
                highScore = score;

            GameData.instance.highScores2[levelID - 9] = highScore;
		}
        s = s.Replace("%s", score.ToString());
        s = s.Replace("%h", highScore.ToString());

        goalText.text = s;
        //Debug.Log("Goal!");
    }

    public void NextLevel()
    {
        GameData.instance.levelID = SceneManager.GetActiveScene().buildIndex + 1;
        GameData.instance.Save();
        LoadingUI.sceneID = GameData.instance.levelID;
        
		goalPanel.SetActive(false);
		
		SceneManager.LoadScene("Loading");

        
    }

    private void UpdateAmmoText()
    {
        for (int i = 0; i < ammoTextObjs.Length; i++)
        {
            ammoTextObjs[i].color = (selectedID == i) ? Color.yellow : Color.white;
        }

        int m = 1;
        if (PlayerGun.doubleAmmo == true)
            m = 2;
        ammoTextObjs[0].text = "Pistol: " + PlayerGun.ammo[0].ToString("D2") + "/" + (PlayerGun.MAX_PISTOL * m).ToString("D2");
        ammoTextObjs[1].text = "Shotgun: " + PlayerGun.ammo[1].ToString("D2") + "/" + (PlayerGun.MAX_SHOTGUN * m).ToString("D2");
        ammoTextObjs[2].text = "Rocket: " + PlayerGun.ammo[2].ToString("D2") + "/" + (PlayerGun.MAX_ROCKET * m).ToString("D2");

        float perc = Health.PlayerHealth / 100f;
        healthOverlay.fillAmount = perc;
        healthOverlay.color = (perc < 0.25f) ? Color.red : new Color(0f, 0.5f, 1f);

        zombieText.text = "Zombies: " + Enemy.aliveZombies;
        keyDont.SetActive(!hasKey);
        keyHave.SetActive(hasKey);
    }
}
