﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public Camera uiCamera;
    public GameObject mainPanel, cheatPassword, cheatPanel;
    public TMPro.TextMeshProUGUI cheatStrText;

    private string cheatStr;
    // Start is called before the first frame update
    void OnEnable()
    {
        uiCamera.cullingMask = LayerMask.GetMask("Player", "UI");
        mainPanel.SetActive(true);
        cheatPanel.SetActive(false);
        cheatPassword.SetActive(false);
        PlayerGun.uiMode = true;
    }
    public void GoToMainPanel()
    {
        mainPanel.SetActive(true);
        cheatPanel.SetActive(false);
        cheatPassword.SetActive(false);
    }

    public void OpenCheatCodes()
    {
        mainPanel.SetActive(false);
        if(PlayerPrefs.GetInt("cheats") == 0)
        {
            cheatStr = "";
            cheatStrText.text = "";
            cheatPassword.SetActive(true);
        }
        else
        {
            cheatPanel.SetActive(true);
            CheatTxt();
        }
    }

    public TMPro.TextMeshProUGUI[] cheatText;

    void CheatTxt()
    {
        int id = 0;
            cheatText[id].text = "Invincibility\n" + ((GameData.instance.cheats[id] == true) ? "On" : "Off");
        id = 1;
            cheatText[id].text = "All Ammo\n" + ((GameData.instance.cheats[id] == true) ? "On" : "Off");
        id = 2;
            cheatText[id].text = "Light Source\n" + ((GameData.instance.cheats[id] == true) ? "On" : "Off");
    }

    public void ToggleCheatID(int id)
    {
        GameData.instance.cheats[id] = !GameData.instance.cheats[id];  //Toggle
        CheatTxt();

        GameObject.FindGameObjectWithTag("Player").SendMessage("UpdateCheat");
    }

    public void RestartLevel()
    {
        LoadingUI.sceneID = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene("Loading");
    }

    public void ReturnToTitle()
    {
        LoadingUI.sceneID = 0;
        SceneManager.LoadScene("Loading");
    }

    private Coroutine cheat;
    public void SetCheatButton(int id)
    {
        if (cheatStr.Length < 4)
        {
            cheatStr += id.ToString();
            cheatStrText.text = cheatStr;
        }
        if(cheatStr.Length == 4)
        {
            if (cheatPlaying == false)
                cheat = StartCoroutine(CheatCode());
        }
        
    }

    public void OpenPatreon()
    {
        Application.OpenURL("https://www.patreon.com/davididev");
    }

    bool cheatPlaying = false;
    IEnumerator CheatCode()
    {
        string correctStr = "7468";  //SHOT
        cheatPlaying = true;
        if (cheatStr == correctStr)
        {
            cheatStrText.text = cheatStr + "\nCorrect!";
            PlayerPrefs.SetInt("cheat", 1);
            PlayerPrefs.Save();
        }
        else
            cheatStrText.text = cheatStr + "\nIncorrect!";

        yield return new WaitForSecondsRealtime(2.5f);

        if(cheatStr == correctStr)
        {
            PlayerPrefs.SetInt("cheats", 1);
            cheatPanel.SetActive(true);
            cheatPassword.SetActive(false);
            CheatTxt();
        }
        cheatStr = "";
        cheatStrText.text = "";
        cheatPlaying = false;
    }

    private void OnDisable()
    {
        uiCamera.cullingMask = LayerMask.GetMask("UI");
        PlayerGun.uiMode = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerGun.isWorking == false)  //Unpause if we lose connection
		{
			Time.timeScale = 1f;
			gameObject.SetActive(false);
		}
    }
}
