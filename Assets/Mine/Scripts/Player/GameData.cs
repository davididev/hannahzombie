﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public static int fileID = 0;
    public static GameData instance = new GameData();

    public int[] highScores = { 0, 0, 0, 0, 0, 0 };
    public int[] highScores2 = { 0, 0, 0, 0, 0, 0, 0 };
    public int[] ammo = { 0, 0, 0 };
    public bool[] cheats = { false, false, false };
    public int levelID = 0;

    

    public GameData()
    {
        ResetFile();
    }

    public void ResetFile()
    {
        highScores = new int[6];
        for (int i = 0; i < highScores.Length; i++)
        {
            highScores[i] = 0;
        }
        highScores2 = new int[7];
        for (int i = 0; i < highScores2.Length; i++)
        {
            highScores2[i] = 0;
        }
        ammo = new int[3];
        cheats = new bool[3];
        levelID = 2;
    }

    public static void NewFile(int id)
    {
        instance.ResetFile();

        fileID = id;
    }

    public void Save()
    {

        for(int i = 0; i < ammo.Length; i++)
        {
            ammo[i] = PlayerGun.ammo[i];
        }
        
        //highScores[levelID] = (Enemy.totalZombies - Enemy.aliveZombies) * 100 / Enemy.totalZombies;
        CSVWriter.Reset();
        CSVWriter.WriteLine(new object[] { "ammo", CSVUtil.IntListToString(ammo, ammo.Length) });
        CSVWriter.WriteLine(new object[] { "hs", CSVUtil.IntListToString(highScores, highScores.Length) });
        CSVWriter.WriteLine(new object[] { "hs2", CSVUtil.IntListToString(highScores2, highScores2.Length) });
        CSVWriter.WriteLine(new object[] { "cheat", CSVUtil.BoolListToString(cheats, cheats.Length) });
        CSVWriter.WriteLine(new object[] { "lvl", levelID});
        CSVWriter.SaveFile(Application.persistentDataPath + "/file" + fileID + ".csv", true);
    }

    public bool Load()
    {
        string[] lines = CSVReader.GetFile(Application.persistentDataPath + "/file" + fileID + ".csv");
        if (lines == null)
        {
            return false;
        }
        for(int i = 0; i < lines[i].Length; i++)
        {
            object[] args = CSVReader.GetArgumentsInLine(lines[i]);
            if ((string)args[0] == "ammo")
            {
                int[] list = CSVUtil.StringToIntList((string)args[1], ammo.Length);
                for (int x = 0; x < list.Length; x++)
                {
                    Debug.Log("Arg: " + list[x]);
                    ammo[x] = list[x];
                    PlayerGun.ammo[x] = ammo[x];
                }
            }
            if ((string)args[0] == "hs")
            {
                int[] list = CSVUtil.StringToIntList((string)args[1], highScores.Length);
                for (int x = 0; x < list.Length; x++)
                {
                    highScores[x] = list[x];
                }
            }
            if ((string)args[0] == "hs2")
            {
                int[] list = CSVUtil.StringToIntList((string)args[1], highScores2.Length);
                for (int x = 0; x < list.Length; x++)
                {
                    highScores2[x] = list[x];
                }
            }
            if ((string)args[0] == "cheat")
            {
                bool[] list = CSVUtil.StringToBoolList((string)args[1], cheats.Length);
                for (int x = 0; x < list.Length; x++)
                {
                    cheats[x] = list[x];
                }
            }

            if ((string)args[0] == "lvl")
                levelID = (int)args[1];
        }
        return true;
    }

}
