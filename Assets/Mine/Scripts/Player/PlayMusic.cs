﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    private AudioSource mainMusic, scaryMusic;

    public void SetupSources(AudioClip main, AudioClip scary)
    {
		if(mainMusic == null)
		{
			mainMusic = gameObject.AddComponent<AudioSource>();
			
			mainMusic.minDistance = 1f;
			mainMusic.loop = true;
		}
		mainMusic.clip = main;

		if(scaryMusic == null)
		{
			scaryMusic = gameObject.AddComponent<AudioSource>();
			
			scaryMusic.minDistance = 1f;
			scaryMusic.loop = true;
			scaryMusic.Stop();
		}
		scaryMusic.clip = scary;
        mainMusic.Play();


    }
    bool lastPlayedScary = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mainMusic != null && scaryMusic != null)
        {
            //Debug.Log("Alert: " + Enemy.alertZombies);
            if (lastPlayedScary == false)  //Currenly playing normal music
            {
                if (Enemy.alertZombies > 0)
                {
                    lastPlayedScary = true;
                    scaryMusic.Play();
                    mainMusic.Pause();
                    return;
                }
            }
            if (lastPlayedScary == true)  //Currently playing scary music
            {
                if (Enemy.alertZombies <= 0)
                {
                    lastPlayedScary = false;
                    scaryMusic.Stop();
                    mainMusic.Play();
                    return;
                }
            }
        }
    }
}
