﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogueHandler : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    [SerializeField] private TMPro.TextMeshProUGUI textObj;
    private Coroutine c;
    public static bool IS_RUNNING = false;
    // Start is called before the first frame update
    void Start()
    {
        textObj.text = "";
        IS_RUNNING = false;   //In case we've changed scenes.
    }

    public void StartDialogue(TextAsset asset)
    {
        if (IS_RUNNING)
            StopCoroutine(c);
        c = StartCoroutine(Dia(asset.text));
    }

    IEnumerator Dia(string s)
    {
        IS_RUNNING = true;
        string[] lines = s.Split('\n');
        for(int i = 0; i < lines.Length; i++)
        {
            string[] args = lines[i].Split(';');
            AudioClip clip = Resources.Load<AudioClip>("Audio/" + args[0]);
			
            if(args[1].Contains("#D"))
			{
				args[1] = args[1].Replace("#D1", "<sprite=\"Diagrams\" index=0>\n");
				args[1] = args[1].Replace("#D2", "<sprite=\"Diagrams\" index=1>\n");
				args[1] = args[1].Replace("#D3", "<sprite=\"Diagrams\" index=2>\n");
				args[1] = args[1].Replace("#D4", "<sprite=\"Diagrams\" index=3>\n");
			}
			textObj.text = args[1];
            source.clip = clip;
            source.Play();
            if (args[0] == "0")  //Just a beep sound
            {
                
                yield return new WaitForSeconds(5f);
				if(args[1].Contains("Chapter complete!"))
				{
					LoadingUI.sceneID = 0;
					SceneManager.LoadScene("Loading");
				}
            }
            else  //Has an actual audio clip
            {
                while (source.isPlaying == true)
                {
                    yield return new WaitForEndOfFrame();
                }
            }

        }
        textObj.text = "";
        IS_RUNNING = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
