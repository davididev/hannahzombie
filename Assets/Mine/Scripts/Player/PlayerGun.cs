﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlayerGun : MonoBehaviour
{
    public static bool isWorking = false, uiMode = false;
    public Transform shootPoint, crosshair;
	private SpriteRenderer crosshairRend;
    public GameObject decalPrefab, bloodSpatterPrefab, bloodExplosionPrefab, smokePistolPrefab, smokeShotgunPrefab, rocketPrefab;
    public Light flashlight, pointLight;
    public AudioClip pistolSoundFX, shotgunSoundFX, rocketSoundFX, sliceFX;

    private int scanPause, scanGameActive;

    private int bulletID = BULLET_PISTOL;

    private const int BULLET_PISTOL = 0, BULLET_SHOTGUN = 1, BULLET_ROCKET = 2;
    private const float DAMAGE_PISTOL = 5f, DAMAGE_SHOTGUN = 25f;

    public const int MAX_PISTOL = 40, MAX_SHOTGUN = 10, MAX_ROCKET = 5;
    public const int STARTING_PISTOL_AMMO = 20;
    public static bool doubleAmmo = false;

	public const float PUSH_FRC = 1f;  //Force multiplier of raygun.

    public static int[] ammo = { 0, 0, 0 };
	private Coroutine lightRoutine;
    public Material pistolDecal, shotgunDecal;
	private bool first = true, lightRoutineRunning = false;
    // Start is called before the first frame update
    void Start()
    {
		if(first)
			SceneManager.activeSceneChanged += this.ResetScene;
		
		
		InitLevel();
    }
	
	public void InitLevel()
	{
		if(lightRoutineRunning == true)
		{
			StopCoroutine(lightRoutine);
			lightRoutineRunning = false;
		}
		ZombieFlame.seed = 0;  //Reset seed
		lightRoutine = StartCoroutine(LightRoutine());
		uiMode = false;
		first = false;
		doubleAmmo = false;
        if(GameData.instance.Load() == false)
        {
            ammo[0] = 0;
            ammo[1] = 0;
            ammo[2] = 0;
        }
	
        if(ammo[0] < STARTING_PISTOL_AMMO)
            ammo[0] = STARTING_PISTOL_AMMO;
        if (ammo[0] > MAX_PISTOL)
            ammo[0] = MAX_PISTOL;
        if (ammo[1] > MAX_SHOTGUN)
            ammo[1] = MAX_SHOTGUN;
        if (ammo[2] > MAX_ROCKET)
            ammo[2] = MAX_ROCKET;
        bulletID = 0;
		PlayerUI.updateText = true;
		

        

        //isWorkng = false;
        scanPause = LayerMask.GetMask("UI", "UI2");
        scanGameActive = LayerMask.GetMask("Default", "Ceiling", "Enemy");
        

		GameObjectPool.InitPoolItem("decal", decalPrefab, 100);
        GameObjectPool.InitPoolItem("blood", bloodSpatterPrefab, 100);
        GameObjectPool.InitPoolItem("blood_death", bloodExplosionPrefab, 10);
        GameObjectPool.InitPoolItem("pistol smoke", smokePistolPrefab, MAX_PISTOL * 2);
        GameObjectPool.InitPoolItem("shotgun smoke", smokeShotgunPrefab, MAX_SHOTGUN * 2);
        GameObjectPool.InitPoolItem("rocket", rocketPrefab, MAX_ROCKET * 2);

	}
	
	void OnDestroy()
	{
		SceneManager.activeSceneChanged -= this.ResetScene;
	}
	
	public void ResetScene(Scene prev, Scene next)
	{
		Invoke("InitLevel", 0.1f);
	}

    IEnumerator Flash()
    {
        pointLight.enabled = true;
        yield return new WaitForSeconds(0.1f);
        pointLight.enabled = false;
    }

    //Set the light intensity (framestep)
    void LightIntensity(float i)
    {
        flashlight.intensity = i;
    }

    /// <summary>
    /// Randomize the flashlight intensity
    /// </summary>
    /// <returns></returns>
    IEnumerator LightRoutine()
    {
		lightRoutineRunning = true;
        while(gameObject)
        {
            float t = 5f;
            float intensity = Random.Range(0.45f, 1f);
            iTween.ValueTo(gameObject, iTween.Hash("from", flashlight.intensity, "to", intensity, "time", t, "onupdate", "LightIntensity"));
            yield return new WaitForSeconds(t);
        }
    }

    private void OnEnable()
    {
        isWorking = true;
    }

    private void OnDisable()
    {
        isWorking = false;
    }

    Button lastButton;
    // Update is called once per frame
    void Update()
    {
        //Scanstep
		if(crosshairRend == null)
			crosshairRend = crosshair.GetComponent<SpriteRenderer>();
        int lm = (uiMode == true) ? scanPause : scanGameActive;
        Vector3 origin = shootPoint.position;
        Vector3 direction = shootPoint.forward;
        RaycastHit info;

        float distance = 90f;
        GameObject g = null;
		crosshairRend.color = Color.white;
        if (Physics.Raycast(origin, direction, out info, distance, lm))
        {
            distance = info.distance - 1f;
            g = info.transform.gameObject;
            if (g.tag == "Enemy")
            {
				crosshairRend.color = Color.red;
                Enemy e = g.GetComponent<Enemy>();
                if(e != null)
                    e.Alert();
            }
			

        }
			


        //Position crosshair
        crosshair.position = origin + (direction * distance);
        crosshair.LookAt(Camera.main.transform.position);

        if(uiMode == true)
        {
            //Game paused
            RaycastHit myInfo; 
            if(Physics.Raycast(origin, direction, out myInfo, 40f, lm))
            {
                //Debug.Log("Scanning " + myInfo.collider.gameObject.name);
                Button b = myInfo.collider.gameObject.GetComponent<Button>();


                if (b != null)  //Scanned a button
                {
                        lastButton = b;
                        //b.GetComponent<Animator>().SetTrigger("Selected");
                        EventSystem.current.SetSelectedGameObject(b.gameObject);
                }
                else  //Didn't scan a button
                {
                    if (lastButton != null)  //Turn off normal button
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        //lastButton.GetComponent<Animator>().SetTrigger("Normal");
                        lastButton = null;
                    }
                }
                if (b != null)
                {
                    if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
                    {
                        b.onClick.Invoke();
                        //b.GetComponent<Animator>().SetTrigger("Pressed");
                    }
                }

            }
        }
        else
        {
            //Game active
        
            //Cycle Weapons
            if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeDownButton))
            {
                CycleWeapons(1);
            }
            if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
            {
                CycleWeapons(-1);
            }

           //Fire Gun
            if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
            {
                //GameData.instance.Save();  //Delete this later, it's for testing.
                if (GameData.instance.cheats[1] == true)
                    ammo[bulletID] = 99;
                if(bulletID > -1 && ammo[bulletID] > 0)
                {
                    
                    ammo[bulletID]--;
                

                    if (bulletID == BULLET_PISTOL)
                    {
						DmgMessage d = new DmgMessage(info.point, info.normal, DAMAGE_PISTOL);
                        GameObject smoke = GameObjectPool.GetInstance("pistol smoke", crosshair.position, Quaternion.identity);
                        if (g != null && g.tag == "Enemy")
                        {
                            
                            g.GetComponent<Health>().Damage(d);

                        }

                        //Draw decal
                        if (g != null)
                        {
                            if (g.layer == LayerMask.NameToLayer("Default") || g.layer == LayerMask.NameToLayer("Ceiling"))
                            {
                                GameObject decal = GameObjectPool.GetInstance("decal", info.point, Quaternion.identity);
                                decal.GetComponent<Decal>().SetupDecal(info.point, info.normal, pistolDecal, 0.25f, 5f, 0.1f, Decal.KillType.INSTANT_HIDE);
                            }
                            Rigidbody rg = g.GetComponent<Rigidbody>();
                            if (rg != null)
                                rg.AddForce(shootPoint.forward * DAMAGE_PISTOL * PUSH_FRC, ForceMode.Impulse);

                        }

                        AudioSource.PlayClipAtPoint(pistolSoundFX, transform.position);
                        if (pointLight.enabled == false)
                            StartCoroutine(Flash());
                    }

                    if (bulletID == BULLET_SHOTGUN)
                    {
						DmgMessage d = new DmgMessage(info.point, info.normal, DAMAGE_SHOTGUN);
                        GameObject smoke = GameObjectPool.GetInstance("shotgun smoke", crosshair.position, Quaternion.identity);
                        if (g != null && g.tag == "Enemy")
                        {
                            
                            g.GetComponent<Health>().Damage(d);
                        }

                        //Draw decal
                        if (g != null)
                        {
                            if (g.layer == LayerMask.NameToLayer("Default") || g.layer == LayerMask.NameToLayer("Ceiling"))
                            {
                                GameObject decal = GameObjectPool.GetInstance("decal", info.point, Quaternion.identity);
                                decal.GetComponent<Decal>().SetupDecal(info.point, info.normal, shotgunDecal, 1f, 5f, 0.1f, Decal.KillType.INSTANT_HIDE);
                            }
                            Rigidbody rg = g.GetComponent<Rigidbody>();
                            if (rg != null)
                                rg.AddForce(shootPoint.forward * DAMAGE_SHOTGUN * PUSH_FRC, ForceMode.Impulse);

                        }

                        AudioSource.PlayClipAtPoint(shotgunSoundFX, transform.position);
                        if (pointLight.enabled == false)
                            StartCoroutine(Flash());
                    }

                    if(bulletID == BULLET_ROCKET)
                    {
                        AudioSource.PlayClipAtPoint(rocketSoundFX, transform.position);
                        Vector3 v = shootPoint.position + (shootPoint.forward * 0.75f);
                        GameObject rock = GameObjectPool.GetInstance("rocket", v, Quaternion.identity);
                            rock.transform.forward = shootPoint.forward;
                        //Launch rocket
                    }

                    if (ammo[bulletID] == 0)  //Just used the last bullet
                        CycleWeapons(-1);
                    PlayerUI.updateText = true;

                }


            }
        }

    }

    /// <summary>
    /// Attempt to add ammo
    /// </summary>
    /// <param name="amt">The amount to add</param>
    /// <param name="kind">Kind of ammo (0 = pistol, 1 = shotgun, 2 = rocket)</param>
    /// <returns>true if added, false if already maxed out.</returns>
    public static bool AddAmmo(int amt, int kind)
    {
        int max = 0;
        if (kind == 0)
            max = MAX_PISTOL;
        if (kind == 1)
            max = MAX_SHOTGUN;
        if (kind == 2)
            max = MAX_ROCKET;

        if (doubleAmmo)
            max *= 2;

        if (ammo[kind] == max)
            return false;
        ammo[kind] += amt;
        if (ammo[kind] > max)
            ammo[kind] = max;

        PlayerUI.updateText = true;
        return true;
    }

    void CycleWeapons(int direction)
    {
        this.bulletID += direction;
        if (bulletID < 0)
            bulletID = BULLET_ROCKET;
        if (bulletID > BULLET_ROCKET)
            bulletID = BULLET_PISTOL;

        if(ammo[bulletID] == 0)
        {
            int totalAmmo = 0;
            for(int i = 0; i < ammo.Length; i++)
            {
                totalAmmo += ammo[i];
            }
            if (totalAmmo > 0)
                CycleWeapons(direction);

            if (totalAmmo == 0)
                bulletID = -1;
        }

        PlayerUI.selectedID = bulletID;
        PlayerUI.updateText = true;

        //Check ammo
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            DmgMessage d = new DmgMessage(shootPoint.position, -shootPoint.forward, 5f);
            other.GetComponent<Health>().Damage(d);
            AudioSource.PlayClipAtPoint(sliceFX, shootPoint.position);
        }
    }
}
