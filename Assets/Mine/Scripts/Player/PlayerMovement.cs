﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform headRotator;
    private CharacterController cc;
    private CharacterControllerMovement ccm;
    public GameObject pauseMenu;
    public Light cheatLight;
	public AudioClip jumpSound;


    public static bool disablePause = false;
    // Start is called before the first frame update
    void Start()
    {
        //disablePause = SceneManager.GetActiveScene().buildIndex < 1;
        SceneManager.activeSceneChanged += this.ResetPos;
    }
	
	void OnDestroy()
	{
		SceneManager.activeSceneChanged -= this.ResetPos;
	}
	
    public void ResetPos(Scene prev, Scene next)
    {
        cc.enabled = false;
        transform.position = Vector3.zero;
        cc.enabled = true;
		deathParticle.Stop();
		deathText.SetActive(false);
		isDead = false;
    }

    bool isDead = false;
    public void OnDeath()
    {
        if(isDead == false)
            StartCoroutine(DeathRoutine());

        isDead = true;
    }

    public GameObject deathText;
    public ParticleSystem deathParticle;

    IEnumerator DeathRoutine()
    {
        deathParticle.Play();
        deathText.SetActive(true);
        yield return new WaitForSeconds(8f);
        LoadingUI.sceneID = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene("Loading");
    }

    public void UpdateCheat()
    {
        GetComponent<Health>().isInvincible = GameData.instance.cheats[0];
        cheatLight.enabled = GameData.instance.cheats[2];
    }

    // Update is called once per frame
    void Update()
    {
        if (cc == null)
            cc = GetComponent<CharacterController>();
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
		
        Vector3 rot = transform.eulerAngles;
        rot.y = HeadRotator.RealFacing;
        transform.eulerAngles = rot;

        Vector3 top = transform.position + cc.center + (Vector3.up * cc.height * 0.4f);
        headRotator.transform.position = top;
        /*
        Vector2 moveVec = Vector2.zero;
        if (Finch.FinchController.Right.SwipeTop)
            moveVec.y += 1f;
        if (Finch.FinchController.Right.SwipeBottom)
            moveVec.y -= 1f;
        if (Finch.FinchController.Right.SwipeRight)
            moveVec.x += 1f;
        if (Finch.FinchController.Right.SwipeLeft)
            moveVec.x -= 1f;
        */
		//Begin controls
		if(PlayerGun.isWorking == true)
		{
			Vector2 moveVec = Vector2.zero;
			/*
			if(moveVec.x > 0f)
				moveVec.x = Mathf.Floor(moveVec.x * 2f) / 2f;
			else
				moveVec.x = Mathf.Ceil(moveVec.x * 2f) / 2f;
			*/
			if(Finch.FinchController.Right.TouchAxes.y > 0.33f)
				moveVec.y = 1f;
			if(Finch.FinchController.Right.TouchAxes.y < -0.33f)
				moveVec.y = -1f;
			
			

            //Jumping
            if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton))
            {
                if (ccm.cc.isGrounded && Mathf.Approximately(Time.timeScale, 1f))
                {
					Vector3 f = (transform.forward * moveVec.y * 10f);
					if(Mathf.Abs(Finch.FinchController.Right.TouchAxes.x) < 0.33f)
					{
						f = f + (Vector3.up * 15f);
					}
					else
					{
						f = f + (Vector3.up * 2.5f);
						float sideJump = 16f;
						if(Finch.FinchController.Right.TouchAxes.x < 0f)
							f = f + (-transform.right * sideJump);
						else
							f = f + (transform.right * sideJump);
					}
					
					if(jumpSound != null)
						AudioSource.PlayClipAtPoint(jumpSound, transform.position);
					
						
					ccm.AddForce(f);
                }
            }

			//Post move vec
			if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.ThumbButton))
				moveVec.x = 0f;
			ccm.moveVec = moveVec;
			//Pause mode
			if (disablePause == false)
			{
				if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.AppButton))
				{
					//Toggle pause
					if (Mathf.Approximately(Time.timeScale, 1f))
					{
						//Pause
						Time.timeScale = 0f;
						//PlayerHand.uiMode = true;
						pauseMenu.SetActive(true);
					}
					else
					{
						//Unpause
						Time.timeScale = 1f;
						//PlayerHand.uiMode = false;
						pauseMenu.SetActive(false);
					}
				}
			}
		}
		//End controls
    }
}
