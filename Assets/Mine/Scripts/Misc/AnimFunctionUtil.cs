﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimFunctionUtil : MonoBehaviour
{
	public ParticleSystem[] psystems;
	public AudioSource[] sounds;
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void PlayParticle1()
	{
		psystems[0].Play();
	}
	
	
	public void PlayParticle2()
	{
		psystems[1].Play();
	}
	
	
	public void PlayParticle3()
	{
		psystems[2].Play();
	}
	
	public void PlaySound1()
	{
		sounds[0].Play();
	}
	
	public void PlaySound2()
	{
		sounds[1].Play();
	}
	
	public void PlaySound3()
	{
		sounds[2].Play();
	}
	
	public void StopSound1()
	{
		sounds[0].Stop();
	}
	
	public void StopSound2()
	{
		sounds[1].Stop();
	}
	
	public void StopSound3()
	{
		sounds[2].Stop();
	}
	
	public void PauseSound1()
	{
		sounds[0].Pause();
	}
	
	public void PauseSound2()
	{
		sounds[1].Pause();
	}
	
	public void PauseSound3()
	{
		sounds[2].Pause();
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
