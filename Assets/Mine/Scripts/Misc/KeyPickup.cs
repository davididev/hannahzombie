﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : MonoBehaviour
{
    public AudioClip gotKeySound;
    private const float ROTATE_PER_SECOND = 90f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rot = transform.eulerAngles;
        rot.y += ROTATE_PER_SECOND * Time.deltaTime;
        transform.eulerAngles = rot;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(gotKeySound, transform.position);
            PlayerUI.hasKey = true;
            PlayerUI.updateText = true;
            gameObject.SetActive(false);
        }
    }
}
