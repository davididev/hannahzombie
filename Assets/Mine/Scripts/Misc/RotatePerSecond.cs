﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePerSecond : MonoBehaviour {

	[SerializeField] public Vector3 rotatePerSecond;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(rotatePerSecond * Time.deltaTime);
	}
}
