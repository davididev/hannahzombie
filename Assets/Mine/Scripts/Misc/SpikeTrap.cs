﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{
	private const float DAMAGE = 40f;
	private const float MOVE_SPEED = 15f;
	private Transform player;
	public AudioClip swipeSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

	private bool isAttacking = false;
    // Update is called once per frame
    void Update()
    {
        if(player == null)
			player = GameObject.FindWithTag("Player").transform;
		
		if(player != null)
		{
			if(isAttacking == false)
			{
				Vector3 v1 = transform.position;
				v1.y = 0f;
				Vector3 v2 = player.position;
				v2.y = 0f;
			
				if(Vector3.Distance(v1, v2) < 4f)
				{
					StartCoroutine(Attack(player.position.y));
				}
			}
		}
    }
	
	IEnumerator Attack(float targetY)
	{
		if(swipeSound != null)
			AudioSource.PlayClipAtPoint(swipeSound, transform.position);
		
		isAttacking = true;
		bool done = false;
		Vector3 startPos = transform.position;
		while(done == false)
		{
			Vector3 targetPos = startPos;
			targetPos.y = targetY;
			
			Vector3 newPos = Vector3.MoveTowards(transform.position, targetPos, MOVE_SPEED * Time.deltaTime);
			transform.position = newPos;
			
			if(Vector3.Distance(newPos, targetPos) < 0.1f)
				done = true;
			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForSeconds(1f);
		
		done = false;
		while(done == false)
		{
			Vector3 targetPos = startPos;
			
			Vector3 newPos = Vector3.MoveTowards(transform.position, targetPos, MOVE_SPEED * Time.deltaTime);
			transform.position = newPos;
			
			if(Vector3.Distance(newPos, targetPos) < 0.1f)
				done = true;
			yield return new WaitForEndOfFrame();
		}
		
		isAttacking = false;
	}
	
	void OnTriggerStay(Collider col)
	{
		Health h = col.GetComponent<Health>();
		CharacterControllerMovement ccm = col.GetComponent<CharacterControllerMovement>();
		
		if(h != null)
		{
			Vector3 v = col.transform.position + (Vector3.up * 1.2f);
			DmgMessage msg = new DmgMessage(v, col.transform.forward, DAMAGE * Time.deltaTime);
			h.Damage(msg);
		}
	}
}
