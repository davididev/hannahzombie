﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTurret : MonoBehaviour
{
	public Transform[] turretPositions;
	public const float DISTANCE = 15f;
	public const float TIMER_BETWEEN_SHOTS = 2f;
	
	private float shotTimer = 0f;
	private int turretID = 0;
	
	private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(player == null)
			player = GameObject.FindWithTag("Player").transform;
		
		if(player != null)
		{
			Vector3 pos1 = transform.position;
			pos1.y = 0f;
			Vector3 pos2 = player.position;
			pos2.y = 0f;
			
			if(PlayerGun.isWorking == true)
				shotTimer += Time.deltaTime;
			
			if(shotTimer >= TIMER_BETWEEN_SHOTS)
			{
				shotTimer -= TIMER_BETWEEN_SHOTS;
				Vector3 v = turretPositions[turretID].position;
				if(Vector3.Distance(pos1, pos2) < DISTANCE)
				{
					GameObject flame = GameObjectPool.GetInstance("enemy flame", v, Quaternion.identity);
					if(flame != null)
					{
						flame.transform.forward = turretPositions[turretID].up;
						turretID++;
					
						if(turretID >= turretPositions.Length)
							turretID = 0;
					}
				}
			}
			
		}
    }
}
