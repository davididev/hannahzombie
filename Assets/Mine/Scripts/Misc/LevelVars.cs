﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelVars : MonoBehaviour
{
    public AudioClip mainMusic, scaryMusic;
    public TextAsset startingDialogue;
	public GameObject boilerplatePrefab;
    // Start is called before the first frame update
    void Start()
    {
        if (FindObjectOfType<BoilerRoot>() == null)
        {
            GameObject g = GameObject.Instantiate(boilerplatePrefab, Vector3.zero, Quaternion.identity) as GameObject;
        }
        FindObjectOfType<PlayMusic>().SetupSources(mainMusic, scaryMusic);

        if (startingDialogue != null)
            StartCoroutine(OpenDia());
    }
	
	void OnDestroy()
	{
		Enemy.totalZombies = 0;
		Enemy.alertZombies = 0;
		Enemy.aliveZombies = 0;
	}

    IEnumerator OpenDia()
    {
        yield return new WaitForEndOfFrame();
        while(PlayerGun.isWorking == false)
        {
            yield return new WaitForSecondsRealtime(1f);
        }

        FindObjectOfType<DialogueHandler>().StartDialogue(startingDialogue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
