﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGoal : MonoBehaviour
{
    public TextAsset ifNoKeyDialogue;
    private const float TIMER = 30f;
    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0f)
            timer -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(PlayerUI.hasKey == false && timer <= 0f)
            {
                timer = TIMER;
                FindObjectOfType<DialogueHandler>().StartDialogue(ifNoKeyDialogue);
            }
            if(PlayerUI.hasKey)
            {
                PlayerUI.goal = true;
                PlayerUI.updateText = true;
            }
        }
    }
}
