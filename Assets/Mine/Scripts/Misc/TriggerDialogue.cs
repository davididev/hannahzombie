﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : MonoBehaviour
{
    public TextAsset tutorialFile;
    public float secondsUntilReplay = 60f;
    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > 0f)
        {
            timer -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && timer <= 0f)
        {
            FindObjectOfType<DialogueHandler>().StartDialogue(tutorialFile);
            timer = secondsUntilReplay;
        }
    }
}
