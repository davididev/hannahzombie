﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSwitch : MonoBehaviour
{
	public AudioClip waterDrainSound;
	public MeshRenderer rend;
	
	private float currentDegrees = 0f;
	private Color c1, c2;
    // Start is called before the first frame update
    void Start()
    {
        c1 = new Color(0f, 0f, 0f, 1f);
		c2 = new Color(0.1f, 0.5f, 0.1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
		currentDegrees += 20f * Time.deltaTime;
		if(currentDegrees > 360f)
			currentDegrees -= 360f;
        rend.material.SetColor("_Emmission", Color.Lerp(c1, c2, Mathf.Cos(currentDegrees * Mathf.Deg2Rad)));
		
		Vector3 rot = transform.eulerAngles;
		rot.y += 25f * Time.deltaTime;
		transform.eulerAngles = rot;

    }
	
	IEnumerator Si()
	{
		SpiritWater.LowerWater = false;
		yield return new WaitForSeconds(0.05f);
		SpiritWater.LowerWater = true;
	}
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			if(SpiritWater.CanReset == true)
			{
				StartCoroutine(Si());
				if(waterDrainSound != null)
					AudioSource.PlayClipAtPoint(waterDrainSound, transform.position);
			}
			
		}
	}
	
}
