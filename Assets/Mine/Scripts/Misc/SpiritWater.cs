﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritWater : MonoBehaviour
{
	public MeshRenderer spiritWater;
	
	private Health playerHealth;
	public static bool CanReset {private set; get; }
	
	private float currentDegrees = 0f;
	private float DAMAGE = 5f, MOVE_SPEED = 1f;
	public float lowerDistance = 6f, lowerTime = 30f;
	
	public static bool LowerWater = false;
	private float lowerTimer = 0f;
	
	private float startingY = 0f, lowerY = 0f;
	
    // Start is called before the first frame update
    void Start()
    {
        startingY = transform.position.y;
		lowerY = startingY - lowerDistance;
		CanReset = true;
		LowerWater = false;
		lowerTimer = 0f;
    }

	void ChangeElevation()
	{
		float newY = 0f;
		if(LowerWater == true)
		{
			//Lower switch has been turned on.
			newY = Mathf.MoveTowards(transform.position.y, lowerY, MOVE_SPEED * Time.deltaTime);
			if(newY == lowerY)
			{
				CanReset = true;
				lowerTimer -= Time.deltaTime;
				if(lowerTimer <= 0f)
					LowerWater = false;
			}
			else
			{
				CanReset = false;
				lowerTimer = lowerTime;
			}
		}
		else  //Lower switch has automatically been turned off
		{
			newY = Mathf.MoveTowards(transform.position.y, startingY, MOVE_SPEED * Time.deltaTime);
			
		}
		
		Vector3 newPos = new Vector3(transform.position.x, newY, transform.position.z);
		transform.position = newPos;
	}

    // Update is called once per frame
    void Update()
    {
        if(playerHealth == null)
			playerHealth = GameObject.FindWithTag("Player").GetComponent<Health>();
		
		ChangeElevation();
		
		currentDegrees += 2.5f * Time.deltaTime;
		
		Vector2 v1 = new Vector2(Mathf.Sin(currentDegrees * Mathf.Deg2Rad), 0f);
		Vector2 v2 = new Vector2(0f, Mathf.Sin(currentDegrees * Mathf.Deg2Rad));
		spiritWater.material.SetTextureOffset("_MainTex", v2);
		spiritWater.material.SetTextureOffset("_OverlapTex", v1);
		
		if(playerHealth != null)
		{
			Vector3 bot = playerHealth.transform.position;
			bot.y -= 0f;
			//Debug.Log("Player: " + bot + "; Water: " + transform.position);
			if(bot.y < transform.position.y)
			{
				playerHealth.Damage(new DmgMessage(playerHealth.transform.position, Vector3.up, DAMAGE * Time.deltaTime));
			}
		}
    }
}
