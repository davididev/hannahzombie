﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AmmoPickup : MonoBehaviour
{
    public int[] amountToAdd = { 0, 0, 0 };
    public bool doublesAmmo = false;

    public AudioClip successSound;
    public static bool firstAmmo = true;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex != Enemy.LEVEL_1_ID)
            firstAmmo = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(firstAmmo)
            {
                TextAsset txt = Resources.Load<TextAsset>("addAmmo");
                FindObjectOfType<DialogueHandler>().StartDialogue(txt);
                firstAmmo = false;
            }
            int x = 0;
            for(int i = 0; i < amountToAdd.Length; i++)
            {
                if (amountToAdd[i] > 0)
                {
                    if (PlayerGun.AddAmmo(amountToAdd[i], i) == true)
                        x++;
                }
            }
            if (doublesAmmo)
                PlayerGun.doubleAmmo = true;
            if (x > 0)
            {
                AudioSource.PlayClipAtPoint(successSound, transform.position);
                gameObject.SetActive(false);
            }
        }
    }
}
