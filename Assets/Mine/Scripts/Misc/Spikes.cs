﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
	private const float DAMAGE = 5f;
	public bool goUpAndDown = false;
	
	private float timer = 0f;
	private int step = 0;
	
	private Vector3 startPos;
	public AudioClip jumpSound;
    // Start is called before the first frame update
    void Start()
    {
		startPos = transform.position;  
    }

    // Update is called once per frame
    void Update()
    {
        if(goUpAndDown == true)
		{
			//Stay up
			if(step == 0)
			{
				timer += Time.deltaTime;
				if(timer >= 4f)
				{
					step = 1;
					timer = 0f;
				}
			}
			//Lower
			if(step == 1)
			{
				Vector3 targetPos = startPos + (Vector3.down * 3f);
				Vector3 newPos = Vector3.MoveTowards(transform.position, targetPos, 1f * Time.deltaTime);
				transform.position = newPos;
				
				if(newPos == targetPos)
					step = 2;
			}
			//Stay lower
			if(step == 2)
			{
				timer += Time.deltaTime;
				if(timer >= 4f)
				{
					step = 3;
					timer = 0f;
				}
			}
			//Raise
			if(step == 3)
			{
				if(jumpSound != null)
					AudioSource.PlayClipAtPoint(jumpSound, startPos);
				Vector3 targetPos = startPos;
				Vector3 newPos = Vector3.MoveTowards(transform.position, targetPos, 10f * Time.deltaTime);
				transform.position = newPos;
				
				if(newPos == targetPos)
					step = 0;
			}
		}
    }
	
	void OnTriggerEnter(Collider col)
	{
		Health h = col.GetComponent<Health>();
		CharacterControllerMovement ccm = col.GetComponent<CharacterControllerMovement>();
		
		if(h != null)
		{
			Vector3 v = col.transform.position + (Vector3.up * 1.2f);
			DmgMessage msg = new DmgMessage(v, Vector3.up, DAMAGE);
			h.Damage(msg);
		}
		if(ccm != null)
		{
			ccm.AddForce(Vector3.up * 25f);
		}
	}
}
