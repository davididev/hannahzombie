﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour
{
    public static int sceneID = 0;

    public TMPro.TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        StartCoroutine(Awww());
    }

    IEnumerator Awww()
    {
        AsyncOperation sync = SceneManager.LoadSceneAsync(sceneID);
		if(sync == null)
			sync = SceneManager.LoadSceneAsync(0);  //Load title
        while(sync.isDone == false)
        {
            text.text = "Loading: " + Mathf.FloorToInt(sync.progress * 100f) + "%";
            yield return new WaitForEndOfFrame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
