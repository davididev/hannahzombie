﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFlame : Enemy
{
    public AudioClip loopWander, loopFollow;
	public ParticleSystem flameParticle;
    AudioSource loop;

	public GameObject fireballPrefab;

    public override void OnDeath()
    {
        base.OnDeath();
		flameParticle.Stop();
        loop.Stop();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if(isAlerted == false)
        {
            if (ScanForPlayer())
                Alert();
        }
    }
	

	public static int seed = 0;
    public override IEnumerator MainRoutine()
    {
        loop = gameObject.AddComponent<AudioSource>();
        loop.minDistance = 1f;
        loop.maxDistance = 20f;
        loop.volume = 0.5f;
        loop.clip = loopWander;
        loop.loop = true;
        loop.spatialBlend = 1f;
        loop.Play();

        SetBrain(BRAIN.Wander);

        while(isAlerted == false)
        {
            yield return new WaitForEndOfFrame();
        }
		GameObjectPool.InitPoolItem("enemy flame", fireballPrefab, 90);
        loop.clip = loopFollow;
        loop.Play();

        SetBrain(BRAIN.Follow);
        while(h.healthPercentage > 0f)
        {
            yield return new WaitForSeconds(1f);
			Vector3 v = transform.position + (Vector3.up * 1.5f); 
			v = v + (transform.forward * 2f);
			GameObject flame = GameObjectPool.GetInstance("enemy flame", v, Quaternion.identity);
			
			if(flame != null)
			{
				Random.InitState(seed);
				float rightOffset = Random.Range(-1f, 1f);
				seed++;
				if(seed >= 100)
					seed = 0;
				Vector3 targetPos = player.position + (Vector3.up * 1.25f) + (player.right * rightOffset);
				flame.transform.LookAt(targetPos);
			}
        }
    }
}
