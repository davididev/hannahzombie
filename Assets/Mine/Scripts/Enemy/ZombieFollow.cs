﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFollow : Enemy
{

    public AudioClip loopWander, loopFollow;
    AudioSource loop;

    public override void OnDeath()
    {
        base.OnDeath();
        loop.Stop();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if(isAlerted == false)
        {
            if (ScanForPlayer())
                Alert();
        }
    }

    public override IEnumerator MainRoutine()
    {
        loop = gameObject.AddComponent<AudioSource>();
        loop.minDistance = 1f;
        loop.maxDistance = 20f;
        loop.volume = 0.5f;
        loop.clip = loopWander;
        loop.loop = true;
        loop.spatialBlend = 1f;
        loop.Play();

        SetBrain(BRAIN.Wander);

        while(isAlerted == false)
        {
            yield return new WaitForEndOfFrame();
        }
        loop.clip = loopFollow;
        loop.Play();

        SetBrain(BRAIN.Follow);
        while(h.healthPercentage > 0f)
        {
            yield return new WaitForEndOfFrame();
        }
    }
}
