﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public const int LEVEL_1_ID = 2;  //Scene ID of level 1
    protected Animator anim;
    protected Transform player;
    protected Health h;
    protected CharacterControllerMovement ccm;

    protected bool isAlerted = false;

    public float touchDamage = 0f;
    private float touchTimer = 0f;

    private Coroutine brain, mainRoutine;
    public static int aliveZombies = 0, alertZombies = 0, totalZombies = 0;
    public static bool firstZombie = true;
    // Start is called before the first frame update
    void OnEnable()
    {
        h = GetComponent<Health>();
        ccm = GetComponent<CharacterControllerMovement>();
        anim = GetComponent<Animator>();
        

        //mainRoutine = StartCoroutine(MainRoutine());
        StartCoroutine(StartMainRoutine());
        StartCoroutine(StartCounter());
		
        
    }
	
	IEnumerator StartCounter()
	{
		yield return new WaitForEndOfFrame();
		totalZombies++;
		aliveZombies++;
        PlayerUI.updateText = true;
	}


    private void OnDisable()
    {
        //if (isAlerted == true)
        //    alertZombies--;
		//It already does this on death

        if (h.healthPercentage > 0f)
            aliveZombies--;
    }

    IEnumerator StartMainRoutine()
    {
        yield return new WaitForEndOfFrame();
        while (PlayerGun.isWorking == false)
        {
            yield return new WaitForEndOfFrame();
        }
        player = GameObject.FindGameObjectWithTag("Player").transform;
        mainRoutine = StartCoroutine(MainRoutine());

        //Only do the first zombie dialogue in Level 1.
        if (SceneManager.GetActiveScene().buildIndex != LEVEL_1_ID)
            firstZombie = false;
    }

    public void Alert()
    {
        if (isAlerted == false)
        {
            isAlerted = true;
            alertZombies++;

            if(firstZombie)
            {
                TextAsset txt = Resources.Load<TextAsset>("shoot");
                FindObjectOfType<DialogueHandler>().StartDialogue(txt);
                firstZombie = false;
            }
        }
    }

    public virtual IEnumerator MainRoutine()
    {
        yield return new WaitForSeconds(1f);
    }

    public void SetBrain(BRAIN b)
    {
        if (brain != null)
            StopCoroutine(brain);
        if(b == BRAIN.None)
        {
            ccm.moveVec.Set(0f, 0f);
        }
        if(b == BRAIN.Wander)
            brain = StartCoroutine(BrainWander());
        if (b == BRAIN.Follow)
            brain = StartCoroutine(BrainFollow());
    }

    public enum BRAIN { None, Wander, Follow}
    private IEnumerator BrainWander()
    {
        Vector3 startingPos = transform.position;
        while(gameObject)
        {
            ccm.moveVec = Vector2.zero;
            yield return new WaitForSeconds(2f);
            float targetAngle = Random.Range(0f, 360f);
            //First
            bool facingTarget = false;
            while(!facingTarget)
            {
                Vector3 rot = transform.eulerAngles;
                rot.y = Mathf.MoveTowardsAngle(rot.y, targetAngle, 360f * Time.deltaTime);
                transform.eulerAngles = rot;
                if (Mathf.Approximately(rot.y, targetAngle))
                    facingTarget = true;

                yield return new WaitForEndOfFrame();
            }


            float newTime = Time.time + 0.5f;
            while(Time.time < newTime)
            {
                ccm.moveVec.y = 1f;
                yield return new WaitForEndOfFrame();
            }

            //Wait before turn back
            ccm.moveVec = Vector2.zero;
            yield return new WaitForSeconds(2f);

            //Turn back
            targetAngle += 180f;
            facingTarget = false;
            while (!facingTarget)
            {
                Vector3 rot = transform.eulerAngles;
                rot.y = Mathf.MoveTowardsAngle(rot.y, targetAngle, 360f * Time.deltaTime);
                transform.eulerAngles = rot;
                if (Mathf.Approximately(rot.y, targetAngle))
                    facingTarget = true;

                yield return new WaitForEndOfFrame();
            }


            newTime = Time.time + 0.5f;
            while (Time.time < newTime)
            {
                ccm.moveVec.y = 1f;
                yield return new WaitForEndOfFrame();
            }
        }
    }

    private IEnumerator BrainFollow()
    {
        Vector3 startingPos = transform.position;
        while (gameObject)
        {
            ccm.moveVec.y = 1f;
            Vector3 rel = player.position - transform.position;
            float angle = Quaternion.LookRotation(rel).eulerAngles.y;

            Vector3 rot = transform.eulerAngles;
            rot.y = Mathf.MoveTowardsAngle(rot.y, angle, 360f * Time.deltaTime);
            transform.eulerAngles = rot;
            yield return new WaitForEndOfFrame();

        }
    }

    /// <summary>
    /// Returns "True" if player found.
    /// </summary>
    /// <returns></returns>
    public bool ScanForPlayer()
    {
        int lm = LayerMask.GetMask("Player", "Default");
        RaycastHit info;
        if(Physics.Raycast(transform.position + Vector3.up, transform.forward, out info, 10f, lm))
        {
            if (info.collider.gameObject.tag == "Player")
                return true;
        }

        return false;

    }

    public virtual void OnDeath()
    {
        ccm.enabled = false;
        ccm.cc.enabled = false;
        aliveZombies--;
        if (isAlerted)
            alertZombies--;

        PlayerUI.updateText = true;
        SetBrain(BRAIN.None);

        if (h.healthPercentage < -0.2f)
        {
            GameObject boom = GameObjectPool.GetInstance("blood_death", transform.position, transform.rotation);
            gameObject.SetActive(false);  //Full on explosion!
        }
    }

    public virtual void OnUpdate()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (touchTimer > 0f)
            touchTimer -= Time.deltaTime;

        if (isAlerted == false)
        {
            if (h.healthPercentage < 1f)
                Alert();
        }
        anim.SetFloat("speed", ccm.currentSpeed);
        anim.SetBool("dead", h.healthPercentage <= 0f);
        OnUpdate();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
		if(Time.timeScale < 0.1f)
			return;  //Don't damage while paused.
        if (touchTimer > 0f)
            return;
        if(hit.gameObject.tag == "Player")
        {
            DmgMessage msg = new DmgMessage(hit.point, hit.normal, touchDamage);
            hit.gameObject.SendMessage("Damage", msg);
            touchTimer = 1f;
        }
    }
}
