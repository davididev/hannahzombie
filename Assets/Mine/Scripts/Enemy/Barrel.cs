﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public GameObject explosionPrefab;
    public string explosionName = "barrelboom1";
    // Start is called before the first frame update
    void Start()
    {
        GameObjectPool.InitPoolItem(explosionName, explosionPrefab, 10);
    }

    public void OnDeath()
    {
        GameObject g = GameObjectPool.GetInstance(explosionName, transform.position, transform.rotation);
        gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
