﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireball : MonoBehaviour
{
	public AudioSource onEnableSound;
	public TrailRenderer trailRend;
	public const float VELOCITY_PER_SECOND = 8f;
	public const float DAMAGE = 5f;
	public const float KILL_TIMER = 12f;
	private Rigidbody rigid;
	
	private float timer = 0f;
	
	
	
    // Start is called before the first frame update
    void OnEnable()
    {
        trailRend.Clear();
		timer = 0f;
		onEnableSound.Play();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(rigid == null)
			rigid = GetComponent<Rigidbody>();
		
		
		rigid.velocity = transform.forward * VELOCITY_PER_SECOND;
		
		timer += Time.fixedDeltaTime;
		if(timer > KILL_TIMER)
			gameObject.SetActive(false);
    }
	
	void OnCollisionEnter(Collision c)
	{
		if(c.gameObject.tag == "Player")
		{
			DmgMessage d = new DmgMessage(transform.position, -transform.forward, DAMAGE);
            c.gameObject.GetComponent<Health>().Damage(d);
		}
		
		gameObject.SetActive(false);
	}
}
